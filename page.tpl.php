<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">
<head>
<meta name="keywords" content="Drupal Theme Template Battlefield America COD Call of Duty Clan Xbox Military Army Armed Forces" />
<meta name="description" content="Soldier - a Drupal theme for military games and clans.  Ideal for COD, Americas Army, Battlefield, etc. " />
<title><?php print $head_title ?></title>
<?php print $head ?>
<?php print soldier_reset_css(); ?>
<?php print $styles ?>
<?php print soldier_ie_css(); ?>
<?php print $scripts ?>
</head>
<body>
<div id="wrap">

  <div id="header">
  <?php print $header; ?>
  <?php if ($logo) : ?>
  <a href="<?php print check_url($front_page) ;?>" title="<?php print $site_name ;?>"><img id="logo" src="<?php print($logo) ?>" alt="<?php print t('Home') ?>" /></a>
  <?php endif; ?>
  <h1><a href="<?php print check_url($front_page) ;?>" title="<?php print $site_name ;?>"><?php print $site_name ; ?></a></h1>
  <h2><?php print $site_slogan ; ?></h2>
  </div>

<?php if (isset($primary_links)) : ?>
  <div id="menu">
  <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
  </div>
<?php endif; ?>

  <div id="page">
    <!-- start #content -->
    <div id="main-content">
    <?php print $messages; ?>
    <?php print $help; ?>
    <?php if ($tabs):?>
      <div id="allTabs"><ul class="tabs clearall"><?php print $tabs; ?></ul>
      <?php if ($tabs2): ?><ul class="tabs clearall"><?php print $tabs2; ?></ul><?php endif; ?>
      <div class="clearall"> </div>           
      </div>     
   <?php endif; ?>                
   <?php if ($mission): ?>
   <p id="mission"><?=$mission?></p>
   <?php endif; ?>          
   <?php print $content ?>
   </div>
<!-- end #content -->

<!-- start #sidebar -->
<?php if ($right): ?>
  <div id="sidebar">
  <?php print $search_box ?>
  <?php print $right ?>
  </div>
<?php endif; ?>

<!-- end #sidebar -->


  <div class="clearall">&nbsp;</div>
  </div>
<!-- end #page -->

  <div id="footer"><?php print $footer_message . $footer ?></div>
<!-- Please do not remove link.  Feel free to re-style if doesnt fit your modifications.  Thanks!  -->
<div id="circuithead">Design by <a href="http://www.garystorey.com">Gary Storey</a></div>

</div>
<?php print $closure ?>
</body>
</html>
