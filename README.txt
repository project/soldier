/***********************************************************************/

/*	Solidier v1.0

/*	A tableless, 2 column fixed width military clan theme for Drupal v6
/***********************************************************************/


To install, extract all the files into "/sites/all/themes".

Feel free to modify as long as the links to my site are not removed.


/***********************************************************************/


Gary Storey

http://drupal.org/user/105240/contact


http://www.garystorey.com
/***********************************************************************/
