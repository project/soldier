<?php

function soldier_reset_css() {
  return '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/reset.css" />';
}

function soldier_ie_css() {
  return '<!--[if lt IE 8]><link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/ie.css" /><![endif]-->';
}

?>