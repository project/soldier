<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <div class="info">
  <?php print $content ?>
  </div>
  <?php if ($submitted): ?>
  <p class="submitted">Posted By <?php print theme('username', $node); ?> on <?php print format_date($node->created); ?> </p>
  <?php endif; ?>
  <div class="meta">
  <?php if ($taxonomy): ?>
    <div class="terms"><?php print $terms ?></div>
  <?php endif;?>
  <?php if ($links): print $links; endif; ?>
    </div>
  <?php print $picture ?>
</div>

