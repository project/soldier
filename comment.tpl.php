<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status; print ' '. $zebra; ?>">
  <?php print $picture ?>
  <h4><?php print $title ?></h4 >
  <div class="info">
    <?php print $content ?>
  </div>
  <?php if ($submitted): ?>
    <p class="submitted"><?php print $submitted; ?></p>
  <?php endif; ?>
  <?php if ($links): ?>
    <?php print $links ?>
  <?php endif; ?>
</div>

